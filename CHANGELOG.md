# 1.0.0 (2021-05-31)


### Features

* **root:** add .husky and setup commitizen ([158c1fd](https://github.com/pengzhanlee/PriorityQueue/commit/158c1fd7b47f50c30107b66032fda77576b3b77c))
* **root:** commitlint ([dfb60b6](https://github.com/pengzhanlee/PriorityQueue/commit/dfb60b6ee2e8ab47ee100957066e2d056b4df5e1))



