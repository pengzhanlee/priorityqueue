import { Comparator, Default_Comparator } from './utils';
import Heap from './Heap';

/**
 * 优先队列
 */
export default class PriorityQueue<T = number> {
  private data: Heap<T>;

  /**
   *
   * @param init 初始值
   * @param comparator 比较器
   */
  constructor(init: T[], comparator: Comparator<T> = Default_Comparator) {
    this.data = new Heap<T>(init, comparator);
  }

  top(): T | undefined {
    return this.data.peak();
  }

  push(value: T): void {
    return this.data.insert(value);
  }

  pop(): T | undefined {
    return this.data.remove();
  }

  empty(): void {
    return this.data.empty();
  }

  size(): number {
    return this.data.size();
  }
}
