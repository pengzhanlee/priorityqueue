/**
 * Heap
 */
import { Comparator, Default_Comparator, swap } from '../utils';

export default class Heap<T = number> {
  private data: T[] = [];

  comparator: Comparator<T>;

  /**
   *
   * @param init 初始值
   * @param comparator 比较器
   */
  constructor(init: T[], comparator: Comparator<T> = Default_Comparator) {
    this.comparator = comparator;
    if (init) {
      this.buildHeap(init);
    }
  }

  private shiftUp(fromIndex: number) {
    while (fromIndex > 0) {
      const parentIndex = Math.floor((fromIndex - 1) / 2);
      if (this.comparator(this.data[parentIndex], this.data[fromIndex]) < 0) {
        swap(this.data, fromIndex, parentIndex);
        fromIndex = parentIndex;
      } else {
        break;
      }
    }
  }

  private shiftDown(fromIndex: number) {
    let leftIndex;
    while ((leftIndex = 2 * fromIndex + 1) < this.size()) {
      let maxIndex = fromIndex;

      if (this.comparator(this.data[leftIndex], this.data[maxIndex]) > 0) {
        maxIndex = leftIndex;
      }

      const rightIndex = leftIndex + 1;
      if (
        rightIndex < this.size() &&
        this.comparator(this.data[rightIndex], this.data[maxIndex]) > 0
      ) {
        maxIndex = rightIndex;
      }

      if (fromIndex !== maxIndex) {
        swap(this.data, fromIndex, maxIndex);
        fromIndex = maxIndex;
      } else {
        break;
      }
    }
  }

  size(): number {
    return this.data.length;
  }

  peak(): T {
    return this.data[0];
  }

  empty(): void {
    this.data = [];
  }

  private buildHeap(arr: T[]) {
    if (!arr || !arr.length) {
      return;
    }
    this.data = arr.concat([]);

    if (this.data.length > 1) {
      let i = Math.floor((arr.length - 1 - 1) / 2);
      for (; i >= 0; i--) {
        this.shiftDown(i);
      }
    }
  }

  insert(value: T): void {
    this.data.push(value);
    this.shiftUp(this.size() - 1);
  }

  remove(): T | undefined {
    if (this.data.length) {
      const returnValue = this.data.shift();
      const last = this.data.pop();
      if (last !== undefined) {
        this.data.unshift(last);
        this.shiftDown(0);
        return returnValue;
      }
    }
  }

  toString(): string {
    return this.data.toString();
  }
}
