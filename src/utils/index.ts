export interface Comparator<T> {
  (a: T, b: T): number;
}

export const Default_Comparator: Comparator<unknown> = (a: unknown, b: unknown) => {
  return (a as number) - (b as number);
};

export const swap = (arr: unknown[], from: number, to: number): void => {
  const temp = arr[to];
  arr[to] = arr[from];
  arr[from] = temp;
};
