import PriorityQueue from '../src';

describe('PriorityQueue', () => {
  let arr: { value: number }[], queue: PriorityQueue<typeof arr[number]>;

  beforeEach(() => {
    arr = [{ value: 20 }, { value: 15 }, { value: 9 }, { value: 31 }, { value: 5 }, { value: 4 }];
    queue = new PriorityQueue<typeof arr[number]>(arr, (a, b) => a.value - b.value);
  });

  test('init', () => {
    expect(queue.size()).toBe(arr.length);
    expect(queue.top()!.value).toBe(31);
  });

  test('add', () => {
    queue.push({ value: 90 });
    expect(queue.size()).toBe(arr.length + 1);
    expect(queue.top()!.value).toBe(90);
  });

  test('remove', () => {
    const pop1 = queue.pop()!;
    expect(queue.size()).toBe(arr.length - 1);
    expect(pop1.value).toBe(31);

    const pop2 = queue.pop()!;
    expect(queue.size()).toBe(arr.length - 2);
    expect(pop2.value).toBe(20);

    expect(queue.top()!.value).toBe(15);
  });

  test('empty', () => {
    queue.empty();
    expect(queue.size()).toBe(0);
    expect(queue.top()).toBeFalsy();
    expect(queue.pop()).toBeFalsy();
  });
});
