import typescript from 'rollup-plugin-typescript2';
import pkg from './package.json';
import nodeResolve from '@rollup/plugin-node-resolve';
import babel from '@rollup/plugin-babel';

const extensions = ['.js', '.ts'];
const input = 'src/index.ts';

// 使用 babel 或 使用 rollup-plugin-typescript2
const useBabel = process.env.babel;

const plugins = [];

if (useBabel) {
  plugins.splice(
    0,
    0,
    ...[
      nodeResolve({
        extensions,
      }),
      babel({
        exclude: 'node_modules/**',
        babelHelpers: 'bundled',
        extensions,
      }),
    ]
  );
} else {
  plugins.push(
    typescript({
      tsconfigOverride: {
        compilerOptions: {
          emitDeclarationOnly: false,
        },
      },
    })
  );
}

export default [
  {
    input,
    output: {
      file: pkg.module,
      format: 'esm',
      sourcemap: true,
    },
    plugins,
  },
  {
    input,
    output: {
      file: pkg.main,
      format: 'cjs',
      sourcemap: true,
    },
    plugins,
  },
];
