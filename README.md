# Priority Queue

基于 Heap 的优先队列

[![codecov](https://codecov.io/gh/pengzhanlee/PriorityQueue/branch/master/graph/badge.svg?token=RUQ4M3U300)](https://codecov.io/gh/pengzhanlee/PriorityQueue)
[![Build Status](https://www.travis-ci.com/pengzhanlee/PriorityQueue.svg?token=sbsDfpqHPC73Xcgr5KN8&branch=master)](https://www.travis-ci.com/pengzhanlee/PriorityQueue)

## build script

- `yarn build` rollup + rollup-plugin-typescript2
- `yarn build:babel` rollup + babel + preset-typescript
- `yarn build:wp` webpack + babel + preset-typescript
- `yarn babel` babel only

